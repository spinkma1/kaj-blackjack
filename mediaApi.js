var player;


function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '360',
    width: '640',
    videoId: 'eyoh-Ku9TCI', 
    playerVars: {
      'autoplay': 1,
      'controls': 1,
      'loop': 1,
      'showinfo': 0,
      'rel': 0,
      'modestbranding': 1
    },
    events: {
      'onReady': onPlayerReady
    }
  });
}


function onPlayerReady(event) {
  event.target.playVideo();
}