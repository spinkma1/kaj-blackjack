const form = document.getElementById('form');
const username = document.getElementById('username');
const password = document.getElementById('bet');
let dealerSentence = document.querySelector('.info h2');
localStorage.clear()
//against XSS
var lt = /</g, 
    gt = />/g, 
    ap = /'/g, 
    ic = /"/g;

username.addEventListener('input', () => {
    username.value = username.value.toString().replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;");
    dealerSentence.innerText = " Good day sir, " + username.value + ". How much do you wish to bet today?";
});

form.addEventListener('submit', e => {
    const isValid = validateInputs();

    if (!isValid) {
        e.preventDefault();
    }
});


const setError = (element, message) => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = message;
    inputControl.classList.add('error');
    inputControl.classList.remove('success');
}

const setSuccess = element => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = '';
    inputControl.classList.add('success');
    inputControl.classList.remove('error');
};

const validateInputs = () => {
    const usernameValue = username.value.trim();
    const betValue = bet.value.trim();
    flag=true;
    if(usernameValue === '') {
        setError(username, 'Name is required');
        flag=false;
    } else if (usernameValue.length < 5 ) {
        setError(username, 'Name must be at least 5 character.');
        flag=false;
    } else if (usernameValue.length > 26 ) {
        setError(username, 'Name must be a maximum of 26 characters long.');
        flag=false;
    } else {
        setSuccess(username);
    }


    if(betValue === '') {
        setError(bet, 'Deposit is required');
        flag=false;
    }else if (isNaN(betValue)) {
        setError(bet, 'Invalid deposit input');
        flag = false;
    } else if (betValue < 100 ) {
        setError(bet, 'Minimum deposit is 100 EUR');
        flag=false;
    } else if (betValue > 1000000 ) {
        setError(bet, 'Maximum deposit is 1.000.000');
        flag=false;
    } else {
        setSuccess(bet);
    }
    return flag;
};

document.querySelector(".input-control").addEventListener("keypress", function(e) {
    if (e.key === "Enter") {
      this.click();
    }
  });