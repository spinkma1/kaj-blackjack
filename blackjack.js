// Function to get exchange rate based on the deposit
function getExchange(deposit) {
  if (navigator.onLine) {
      fetch('https://api.api-ninjas.com/v1/convertcurrency?want=USD&have=EUR&amount=' + deposit, {
              method: 'GET',
              headers: {
                  'X-Api-Key': 'e87ZXGw3kOILxHG4H9kBFg==etewp0PSJ9SivW20',
                  'Content-Type': 'application/json'
              }
          })
          .then(response => {
              if (!response.ok) {
                  throw new Error('Network is down');
              }
              return response.json();
          })
          .then(data => {
              const exchangeRateElement = document.getElementById('exchangeRate');
              exchangeRateElement.textContent = `Exchange rate: ${data.old_amount} EUR -> ${data.new_amount} USD`;
          })
          .catch(error => {
              console.error('Error:', error);
          });
  }
}
// Function to get the current exchange rate
function getExchangeRate() {
  if (navigator.onLine) {
      fetch('https://api.api-ninjas.com/v1/exchangerate?pair=USD_EUR', {
              method: 'GET',
              headers: {
                  'X-Api-Key': 'e87ZXGw3kOILxHG4H9kBFg==etewp0PSJ9SivW20',
                  'Content-Type': 'application/json'
              }
          })
          .then(response => {
              if (!response.ok) {
                  throw new Error('Network is down');
              }
              return response.json();
          })
          .then(data => {
              const exchangeRateElement = document.getElementById('exchangeRate');
              exchangeRateElement.textContent = `Exchange rate: ${data.currency_pair} -> ${data.exchange_rate}`;
          })
          .catch(error => {
              console.error('Error:', error);
          });
  }
}
// Initializing variables for DOM elements and game logic
const returnButton = document.getElementById("returnButton")
const dealerCountH1 = document.querySelector("#dealer-count h1")
const playerCountH1 = document.querySelector("#player-count h1")
const playerInfoH1 = document.querySelector("#player-info h1")
const dealerHand = document.getElementById("dealer-hand")
const playerHand = document.getElementById("player-hand")
const confirmButton = document.querySelector('#confirmButton')
const betInput = document.getElementById('bet')
const dealerInfoH1 = document.querySelector('#dealer-info h1')
var cardSound = document.getElementById("cardSound");

var form = document.getElementById("form");
var info = document.querySelector(".info")


// Retrieving URL parameters from lobby.html
const urlParams = new URLSearchParams(window.location.search);
const username = urlParams.get('username');
let deposit = urlParams.get('bet');
deposit = parseInt(deposit)
var balanceEl = document.querySelector('.title');
balanceEl.textContent = "Ace21 Casino " + deposit + " €";
// Displaying balance on the webpage and storing data in local storage
if (!localStorage.getItem("name") || !localStorage.getItem("deposit")) {
  localStorage.setItem('name', username);
  localStorage.setItem('deposit', deposit);
}
const blackjackButtons = document.querySelectorAll('.blackjackButtons');
const leaveButton = document.getElementById("leaveButton")
// Defining constants for card suits and ranks
const SUITS = ['♠', '♣', '♥', '♦'];
const RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];




class Card {
  constructor(rank, suit) {
      this.rank = rank;
      this.suit = suit;
  }

  // Method to get the numerical value of the card
  getValue() {
      if (this.rank === 'A') {
          return 11;
      } else if (['J', 'Q', 'K'].includes(this.rank)) {
          return 10;
      } else {
          return parseInt(this.rank);
      }
  }
  // Method to get the SVG representation of the card
  getSVG() {
      let color = "black";
      switch (this.suit) {
          case '♦':
              color = "red";
              break;
          case '♥':
              color = "red";
              break;
          default:
              color = "black";
      }
      return `<svg width="100" height="150" class="flipInY">
  <rect width="100" height="150" style="fill:white;stroke:black;stroke-width:2"></rect>
  <text x="10" y="30" font-family="Roboto" font-size="30" fill="${color}">${this.rank}</text>
  <text x="10" y="60" font-family="Roboto" font-size="30" fill="${color}">${this.suit}</text>
</svg>`
  }
}

class Deck {
  constructor() {
      this.cards = [];
      for (let suit of SUITS) {
          for (let rank of RANKS) {
              this.cards.push(new Card(rank, suit));
          }
      }
      this.shuffle();
  }

  shuffle() {
      for (let i = this.cards.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [this.cards[i], this.cards[j]] = [this.cards[j], this.cards[i]];
      }
  }

  deal() {
      cardSound.play();
      return this.cards.pop();
  }
  reset() {
      this.cards = [];
      for (let suit of SUITS) {
          for (let rank of RANKS) {
              this.cards.push(new Card(rank, suit));
          }
      }
      this.shuffle();
  }
}

class Player {
  constructor(name, balance) {
      this.name = name;
      this.balance = balance;
      this.hand = [];
      this.won = null;
  }


  addToHand(card) {
      this.hand.push(card)
      console.log(card.rank)
      playerHand.innerHTML += card.getSVG();
      setTimeout(() => {
        const flipElements = playerHand.querySelectorAll(".flipInY");
        flipElements.forEach(element => {
        element.classList.remove("flipInY");
        });
    }, 1000);
      playerCountH1.innerHTML = `<strong>${this.getHandValue()}</strong>`;

  }

  getHandValue() {
      let sum = 0;
      let numAces = 0;
      for (let card of this.hand) {
          sum += card.getValue();
          if (card.rank === 'A') {
              numAces++;
          }
      }
      while (sum > 21 && numAces > 0) {
          sum -= 10;
          numAces--;
      }
      return sum;
  }
}

class Dealer extends Player {
  constructor() {
      super("Dealer", 0);
  }
  play(deck, playerHandValue) {
      while (this.getHandValue() < 17 || (this.getHandValue() <= playerHandValue && this.getHandValue() < 21)) {
          this.addToHand(deck.deal());
      }
  }
  addToHand(card) {
      this.hand.push(card); {
          dealerHand.innerHTML += card.getSVG();
          dealerCountH1.innerHTML = `<strong>${this.getHandValue()}</strong>`;
      }
  }
}

// Creating instances of Deck, Player, and Dealer
const deck = new Deck();
const player = new Player(username, deposit);
const dealer = new Dealer();

// Function for player to "hit" (receive another card)
function hit() {
  if (player.getHandValue() < 22) {
    setTimeout(function() {
      player.addToHand(deck.deal());
  }, 1000);
  }

  setTimeout(function() {
    if (player.getHandValue() > 21) {
        endGame();
    }
  }, 1000);
}
// Function for player to "stand" (keep their current hand)
function stand() {
  while (dealer.getHandValue() < 17) {
    dealer.addToHand(deck.deal());
  }
  endGame();
}
// Function to end the game and determine the winner
function endGame() {
  var deposit = localStorage.getItem("deposit")
  deposit = parseInt(deposit)
  const playerHandValue = player.getHandValue();
  const dealerHandValue = dealer.getHandValue();
  let bet = localStorage.getItem('betAmount');
  if (playerHandValue > 21) {
      dealerInfoH1.innerText = "Dealer wins!";
      dealerInfoH1.style.color = "red";
      dealer.won == true
      player.balance -= parseInt(bet)
  } else if (dealerHandValue > 21 || playerHandValue > dealerHandValue) {
      playerInfoH1.innerText = username + " wins!";
      playerInfoH1.style.color = "red";
      player.won = true
      player.balance += 2 * parseInt(bet)
      deposit += 2 * parseInt(bet);
      localStorage.setItem('deposit', deposit);
  } else if (dealerHandValue > playerHandValue) {
      dealerInfoH1.innerText = "Dealer wins!";
      dealerInfoH1.style.color = "red";
      dealer.won = true
  } else {
      dealerInfoH1.innerText = "Tie!";
      playerInfoH1.innerText = "Tie!";
      dealerInfoH1.style.color = "red";
      playerInfoH1.style.color = "red";
      player.won = false
      dealer.won = false
      player.balance += parseInt(bet)
      deposit += parseInt(bet);
      localStorage.setItem('deposit', deposit);
  }
  blackjackButtons.forEach(button => {
      button.removeEventListener('click', nextMove);
  });
  returnButton.style.display = "flex"
  blackjackButtons.forEach(button => {
      console.log("přidano")
      button.style.display = 'none';
  });
  updateBalance()
}
// Function to reset the game
function resetGame() {
  form.style.display = "flex"
  returnButton.style.display = "none"
  playerInfoH1.innerText = ""
  dealerCountH1.innerText = ""
  playerCountH1.innerText = ""
  dealerInfoH1.innerText = ""
  dealerHand.innerText = ""
  playerHand.innerText = ""
  confirmButton.style.display = 'flex';
  betInput.style.visibility = 'visible';
  dealerInfoH1.innerText = "Please, place your bet.";
  dealerInfoH1.style.color = "white";
  playerInfoH1.style.color = "white";
  player.won = null
  dealer.won = null
  localStorage.setItem("betAmount", "0")
  confirmButton.style.justifyContent = "center"
  getExchangeRate()
}

function placeBet() {
  const isValid = validateInputs();

  if (!isValid) {
      return
  }

  form.style.display = "none"
  confirmButton.style.display = 'none';
  var betAmount = betInput.value;
  localStorage.setItem('betAmount', betAmount);


  var deposit = localStorage.getItem("deposit");
  deposit = parseInt(deposit);
  deposit -= parseInt(betAmount);
  getExchange(deposit)
  localStorage.setItem('deposit', deposit);
  player.balance -= parseInt(betAmount)
  updateBalance()
  betInput.style.visibility = 'hidden';

  blackjackButtons.forEach(button => {
      button.style.display = 'flex';
  });
  playerInfoH1.innerText = username;
  dealerInfoH1.innerText = "Dealer";
  blackjackButtons.forEach(button => {
      button.addEventListener('click', nextMove);
  });
  deck.reset()
  player.hand = []
  dealer.hand = []
  player.addToHand(deck.deal());
 

  setTimeout(function() {
    dealer.addToHand(deck.deal());
  }, 1000);

  setTimeout(function() {
    player.addToHand(deck.deal());
  }, 2000);
  }
// Function to update balance displayed on the webpage
function updateBalance() {
  let deposit = localStorage.getItem("deposit")
  if (deposit != null) {
      var balanceEl = document.querySelector('.title');
      balanceEl.textContent = "Ace21 Casino " + deposit + " €";
  }
}
// Function to handle player's next move (hit or stand)
function nextMove() {
  if (this.textContent === 'Hit') {
      if (player.won == null && dealer.won == null) {
          hit();
      }
  } else if (this.textContent === 'Stand') {
      if (player.won == null && dealer.won == null) {
          stand()
      }
  }
}

// Function to set error message for input validation
const setError = (element, message) => {
  const inputControl = element.parentElement;
  const errorDisplay = inputControl.querySelector('.error');

  errorDisplay.innerText = message;
  inputControl.classList.add('error');
  inputControl.classList.remove('success');
}
// Function to set success message for input validation
const setSuccess = element => {
  const inputControl = element.parentElement;
  const errorDisplay = inputControl.querySelector('.error');

  errorDisplay.innerText = '';
  inputControl.classList.add('success');
  inputControl.classList.remove('error');
};
// Function to set success message for input validation
const validateInputs = () => {
  const betValue = bet.value.trim();
  flag = true;
  var deposit = localStorage.getItem("deposit")
  deposit = parseInt(deposit)
  if (betValue === '') {
      setError(bet, 'Bet is required');
      flag = false;
  } else if ((isNaN(betValue))) {
        setError(bet, 'Invalid bet input');
        flag = false;
  } else if (betValue < 10) {
      setError(bet, 'Minimum bet is 10 EUR');
      flag = false;
  } else if (betValue > 10000) {
      setError(bet, 'Maximum bet is 10.000');
      flag = false;
  } else if (betValue > deposit) {
      setError(bet, 'Bet is greater than balance');
      flag = false;
  } else {
      setSuccess(bet);
  }
  return flag;
};

// Event listener to handle enter key press for bet input
betInput.addEventListener("keyup", function(event) {
  if (event.key === "Enter") {
      confirmButton.click();
  }
});